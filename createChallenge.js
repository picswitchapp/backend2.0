/*Creates a RandomChallenge. 
Requires: memberIds - The objectId’s of the members of this challenge
          timeLimit - Time limit on the challenge, in hours
          category - Category of the challenge

Returns: does not return*/



Parse.Cloud.define("createRandomChallenge", function(request, response) {
  var creator = request.user;
  var memberIds = request.params.memberIds;
  var timeLimit = request.params.timeLimit;
  var category = request.params.category;
  var numberOfPhotos = 3
  var retrievedPhotoArray = [] //to record the objectId's of the photos retrieved
    
  if (!sender) {
    response.error("Error: not signed in.");
  }
  

  /*Query to get x photos from RandomChallenges. x depends on the number of 
  members as speccified in the design doc. numberOfPhotos = x*/
  var randomChallenge = Parse.Object.extend("RandomChallenges");
  var query = new Parse.Query(randomChallenge);
  query.equalTo("category", "category");
  query.notEqualTo("solvers", memberIds); //should be changed in the future to accomodate 
  //more than one member
  query.notEqualTo("solvers", creator.id);
  query.limit(numberOfPhotos); //limit the number of photos retrieved to 3 (Will be changed for groups)
  query.find({
    success: function(randomPhotos) {
      alert("Successfully retrieved " + randomPhotos.length + " RandomChallenges.");
      
      for (var i = 0; i < randomPhotos.length; i++) {
        var object = randomPhotos[i];
        retrievedPhotoArray.push(object.id);
      }
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });



  /*Creating a Challenge object.*/
  var Challenge = Parse.object.extend("Challenge");
  var challenge = new Challenge();
  challenge.set("creator", creator.id);
  var memberRelation = challenge.relation("members");
  memberRelation.add(memberIds); //might need changing with more members
  challenge.set("endTime", timeLimit);
  
  challenge.set("random", true);
  challenge.set("currentRound", 1);
  


  /*Creating a Photo object.*/
  var Photo = Parse.object.extend("Photo");
  var photo = new Photo();
  photo.set("user", creator.id);
  photo.set("location", "RandomChallenges");
  photo.add("photos", retrievedPhotoArray);
  photo.set("round", 1);
  photo.save(null, {
    success: function(photo) {
      alert('New object created with objectId: ' + photo.id);
      /*Challenge object saved after Photo object saved*/
      challenge.set("photo", photo.id); //needs to be nested or chained
      challenge.save(null, {
      success: function(challenge) {
        alert('New object created with objectId: ' + challenge.id);
      },
      error: function(challenge, error) {
        // Execute any logic that should take place if the save fails.
        // error is a Parse.Error with an error code and message.
        alert('Failed to create new object, with error code: ' + error.message);
      }
    });
    },
    error: function(photo, error) {
      // Execute any logic that should take place if the save fails.
      // error is a Parse.Error with an error code and message.
      alert('Failed to create new object, with error code: ' + error.message);
    }
  });
  
});






Parse.Cloud.define("createCustomChallenge", function(request, response) {
  var creator = request.user;
  var memberIds = request.params.memberIds;
  var timeLimit = request.params.timeLimit;
  var photo = new Parse.File("image.jpg", {base64: request.params.photo});

    
  if (!sender) {
    response.error("Error: not signed in.");
  }
  

  /*Creating a Challenge object.*/
  var Challenge = Parse.object.extend("Challenge");
  var challenge = new Challenge();
  challenge.set("creator", creator.id);
  var memberRelation = challenge.relation("members");
  memberRelation.add(memberIds); //might need changing with more members
  challenge.set("endTime", timeLimit);
  
  challenge.set("random", true);
  challenge.set("currentRound", 1);



  /*Creating a Photo object.*/
  var Photo = Parse.object.extend("Photo");
  var photo = new Photo();
  photo.set("user", creator.id);
  photo.set("location", "CustomPhotos");
  
  photo.set("round", 1);
  


  /*Creating a CustomPhotos object.*/
  var CustomPhotos = Parse.object.extend("CustomPhotos");
  var customPhoto = new CustomPhotos();
  customPhoto.set("photo", photo);
  customPhoto.save(null, {
    success: function(customPhoto) {
      alert('New object created with objectId: ' + customPhoto.id);
      photo.add("photos", customPhoto.id);
      photo.save(null, {
      success: function(photo) {
        alert('New object created with objectId: ' + photo.id);
        /*Challenge object saved after Photo object saved*/
        challenge.set("photo", photo.id); //needs to be nested or chained
        challenge.save(null, {
        success: function(challenge) {
          alert('New object created with objectId: ' + challenge.id);
        },
        error: function(challenge, error) {
          // Execute any logic that should take place if the save fails.
          // error is a Parse.Error with an error code and message.
          alert('Failed to create new object, with error code: ' + error.message);
        }
      });
      },
      error: function(photo, error) {
        // Execute any logic that should take place if the save fails.
        // error is a Parse.Error with an error code and message.
        alert('Failed to create new object, with error code: ' + error.message);
      }
    });

    },
    error: function(customPhoto, error) {
      // Execute any logic that should take place if the save fails.
      // error is a Parse.Error with an error code and message.
      alert('Failed to create new object, with error code: ' + error.message);
    }
  });


  


  
  
});
  
  