/***********************************************************************************************
*  PICSWITCH TECHNOLOGIES CONFIDENTIAL
*  ___________________________________
* 
*  PICSWITCH TECHNOLOGIES INC. 
*  Copyright (c) 2016 All Right Reserved.
* 
*
* Program           : PicSwitch Cloud Functions
*
* Authors           : Gibson Mulonga, Jiacong Xu
*
* Date created      : 10th January, 2016
*
* Purpose           : Handle push notifications
*                    
*
* 
*
* MAJOR Revision History  :
*
* Version       Date(DDMMYYYY format)       Author                  Revision  
* v1			15012016					Gibson					Initial commit, createRandomChallege done
************************************************************************************************/
   
/**
 * Loading modules
 */
require("cloud/createChallenge.js");

  

